package Activity4.activity4.inheritance;

public class ElectronicBook extends Book {
    private int numberBytes;

    public ElectronicBook(String title, String author, int numberBytes) {
        super(title,author);
        this.numberBytes = numberBytes;
    }

    public int getNumberBytes() {
        return this.numberBytes;
    }

    public String toString() {
        return super.toString() + " size in bytes: " + this.numberBytes;
    }

    
}